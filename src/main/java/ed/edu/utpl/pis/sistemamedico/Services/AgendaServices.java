package ed.edu.utpl.pis.sistemamedico.Services;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IAgendaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Agenda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")

public class AgendaServices {

    @Autowired
    private IAgendaServices agendaService;

    @GetMapping("/agenda")
    @ResponseStatus(HttpStatus.OK)
    public List<Agenda> getAll(){
        return agendaService.findAll();
    }


    @GetMapping("/agenda/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Agenda getById(@PathVariable Long id){
        return agendaService.findById(id);
    }

    @PostMapping("/agenda")
    @ResponseStatus(HttpStatus.CREATED)
    public Agenda save(@RequestBody Agenda agendas) {
        return agendaService.save(agendas);
    }

    @PutMapping("/agenda/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Agenda> updateById(Long id, @Validated @RequestBody Agenda agendasDetails){
        Agenda agenda =
                agendaService
                        .findById(id);
        agenda.setNombreAgenda(agendasDetails.getNombreAgenda());
        final Agenda updatedAgenda = agendaService.save(agenda);
        return ResponseEntity.ok(updatedAgenda);
    }

    @DeleteMapping("/agenda/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Boolean> deleteById(Long id){
        agendaService.deleteId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}