package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;
import ed.edu.utpl.pis.sistemamedico.Model.Medico;

public interface IMedicoServices {
	public List<Medico> findAll();
	public Medico findById(Long id);
	public Medico save(Medico medico);
	public void deleteId(Long id);
	public Long getId();
}
