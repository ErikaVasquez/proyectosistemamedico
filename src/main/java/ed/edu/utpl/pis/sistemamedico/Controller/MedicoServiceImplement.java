package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.MedicoRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IMedicoServices;
import ed.edu.utpl.pis.sistemamedico.Model.Medico;

@Service
public class MedicoServiceImplement implements IMedicoServices{

	@Autowired
	private MedicoRepository medicoRepository;
	
	@Override
	public List<Medico> findAll() {
		// TODO Auto-generated method stub
		return (List<Medico>)medicoRepository.findAll();
	}

	@Override
	public Medico findById(Long id) {
		// TODO Auto-generated method stub
		return medicoRepository.findById(id).orElse(null);
	}

	@Override
	public Medico save(Medico medico) {
		// TODO Auto-generated method stub
		return medicoRepository.save(medico);
	}

	@Override
	public void deleteId(Long id) {
		medicoRepository.deleteById(id);
		
	}

	public Long getId(){
		Long id = medicoRepository.getId();
		return id;
	}

}
