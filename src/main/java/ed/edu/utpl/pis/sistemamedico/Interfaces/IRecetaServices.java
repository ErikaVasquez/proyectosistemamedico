package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Receta;

public interface IRecetaServices {
	public List<Receta> findAll();
    public Receta findById(Long id);
    public Receta save(Receta receta);
    public void deleteId(Long id);
}
