package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Examen;

public interface ExamenRepository extends CrudRepository<Examen, Long>{

}
