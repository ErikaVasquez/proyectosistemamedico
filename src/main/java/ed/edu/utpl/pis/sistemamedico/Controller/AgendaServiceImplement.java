package ed.edu.utpl.pis.sistemamedico.Controller;

import ed.edu.utpl.pis.sistemamedico.DataRepository.AgendaRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IAgendaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Agenda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgendaServiceImplement implements IAgendaServices {

    @Autowired
    private AgendaRepository agendaRepository;

    public List<Agenda> findAll() {
        return (List<Agenda>)agendaRepository.findAll();
    }

    @Override
    public Agenda findById(Long id) {
        return agendaRepository.findById(id).orElse(null);
    }

    @Override
    public Agenda save(Agenda agenda) {
        return agendaRepository.save(agenda);
    }

    @Override
    public void deleteId(Long id) {
        agendaRepository.deleteById(id);
    }
}