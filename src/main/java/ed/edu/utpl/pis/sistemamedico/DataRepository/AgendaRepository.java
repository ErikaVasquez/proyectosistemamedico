package ed.edu.utpl.pis.sistemamedico.DataRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Agenda;
import org.springframework.data.repository.CrudRepository;

public interface AgendaRepository extends CrudRepository<Agenda, Long> {
}
