package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IUsuarioServices;
import ed.edu.utpl.pis.sistemamedico.Model.Usuario;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1") 
public class UsuarioServices {
	
	@Autowired
	private IUsuarioServices usuarioService;
	
	@GetMapping("/usuarios")
	@ResponseStatus(HttpStatus.OK)
	public List<Usuario> getAll(){
		return usuarioService.findAll();
	}
	

	@GetMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Usuario getById(@PathVariable(value = "id") Long id){
		return usuarioService.findById(id);
	  }
	  
	 @PostMapping("/usuarios")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Usuario save(@RequestBody Usuario usuarios) {
	    return usuarioService.save(usuarios);
	 }
	
	 @PutMapping("/usuarios/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Usuario> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Usuario usuariosDetails){
		  Usuario usuarios =
	        usuarioService
	            .findById(id);
		usuarios.setNombresUsuario(usuariosDetails.getNombresUsuario());
		usuarios.setApellidosUsuario(usuariosDetails.getApellidosUsuario());
		usuarios.setFechaNacimientoUsuario(usuariosDetails.getFechaNacimientoUsuario());
		//usuarios.setRol(usuariosDetails.getRol());
	    usuarios.setDireccionUsuario(usuariosDetails.getDireccionUsuario());
	    usuarios.setTelefonoUsuario(usuariosDetails.getTelefonoUsuario());
	    usuarios.setUserUsuario(usuariosDetails.getUserUsuario());
	    usuarios.setContrasenaUsuario(usuariosDetails.getContrasenaUsuario());
	    final Usuario updatedUsuarios = usuarioService.save(usuarios);
	    return ResponseEntity.ok(updatedUsuarios);
	  }
	
	  @DeleteMapping("/usuarios/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  usuarioService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }	 
	  
	  @GetMapping("/usuarios/login/{username}")
	  @ResponseStatus(HttpStatus.OK)
	  public Usuario getLogin(@PathVariable(value = "username") String username) {
		  return usuarioService.getLogin(username);
	  }
	  
}
