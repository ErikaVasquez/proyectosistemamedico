package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.HistoriaClinicaRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IHistoriaClinicaServices;
import ed.edu.utpl.pis.sistemamedico.Model.HistoriaClinica;

@Service
public class HistoriaClinicaServiceImplement implements IHistoriaClinicaServices{
	@Autowired
	private HistoriaClinicaRepository historiaClinicaRepository;

	@Override
	public List<HistoriaClinica> findAll() {
		// TODO Auto-generated method stub
		return (List<HistoriaClinica>)historiaClinicaRepository.findAll();
	}

	@Override
	public HistoriaClinica findById(Long id) {
		// TODO Auto-generated method stub
		return historiaClinicaRepository.findById(id).orElse(null);
	}

	@Override
	public HistoriaClinica save(HistoriaClinica historiaClinica) {
		// TODO Auto-generated method stub
		return historiaClinicaRepository.save(historiaClinica);
	}

	@Override
	public void deleteId(Long id) {
		historiaClinicaRepository.deleteById(id);
		
	}

}
