package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.EspecialidadRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IEspecialidadServices;
import ed.edu.utpl.pis.sistemamedico.Model.Especialidad;

@Service
public class EspecialidadServiceImplement implements IEspecialidadServices{
	@Autowired
	private EspecialidadRepository especialidadRepository;

	@Override
	public List<Especialidad> findAll() {
		return (List<Especialidad>)especialidadRepository.findAll();
	}

	@Override
	public Especialidad findById(Long id) {
		return especialidadRepository.findById(id).orElse(null);
	}

	@Override
	public Especialidad save(Especialidad especialidad) {
		return especialidadRepository.save(especialidad);
	}

	@Override
	public void deleteId(Long id) {
		especialidadRepository.deleteById(id);
		
	}

	
}
