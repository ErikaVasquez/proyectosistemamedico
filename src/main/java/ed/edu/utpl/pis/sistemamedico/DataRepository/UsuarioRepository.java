package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ed.edu.utpl.pis.sistemamedico.Model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	@Query(value ="SELECT * FROM sistemamedico.usuario e WHERE e.user_usuario =(:username)",nativeQuery = true)
    public Usuario getLogin(@Param("username") String username);
}

