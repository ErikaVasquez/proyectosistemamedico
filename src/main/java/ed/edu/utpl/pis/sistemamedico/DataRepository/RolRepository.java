package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Rol;

public interface RolRepository extends CrudRepository<Rol, Long>{

}
