package ed.edu.utpl.pis.sistemamedico.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "examen")
public class Examen {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "nombre_examen", nullable = false)
    private String nombreExamen;
	
    @Column(name = "descripcion_examen", nullable = false)
    private String descripcionExamen;
    	
    @Column(name = "costo_examen", nullable = false)
    private Double costoExamen;
    
    @Column(name = "tipo_examen", nullable = false)
    private String tipoExamen;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreExamen() {
		return nombreExamen;
	}

	public void setNombreExamen(String nombreExamen) {
		this.nombreExamen = nombreExamen;
	}

	public String getDescripcionExamen() {
		return descripcionExamen;
	}

	public void setDescripcionExamen(String descripcionExamen) {
		this.descripcionExamen = descripcionExamen;
	}

	public Double getCostoExamen() {
		return costoExamen;
	}

	public void setCostoExamen(Double costoExamen) {
		this.costoExamen = costoExamen;
	}

	public String getTipoExamen() {
		return tipoExamen;
	}

	public void setTipoExamen(String tipoExamen) {
		this.tipoExamen = tipoExamen;
	} 
    
}
