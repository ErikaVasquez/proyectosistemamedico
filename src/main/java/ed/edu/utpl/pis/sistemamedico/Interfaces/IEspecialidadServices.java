package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Especialidad;

public interface IEspecialidadServices {
	public List<Especialidad> findAll();
	public Especialidad findById(Long id);
	public Especialidad save(Especialidad especialidad);
	public void deleteId(Long id);
}
