package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Receta;


public interface RecetaRepository extends CrudRepository<Receta, Long>{

}
