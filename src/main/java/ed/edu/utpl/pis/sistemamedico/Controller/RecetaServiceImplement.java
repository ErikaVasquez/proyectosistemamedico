package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.RecetaRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IRecetaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Receta;

@Service
public class RecetaServiceImplement implements IRecetaServices{
	@Autowired
	private RecetaRepository recetaRepository;

	@Override
	public List<Receta> findAll() {
		// TODO Auto-generated method stub
		return (List<Receta>)recetaRepository.findAll();
	}

	@Override
	public Receta findById(Long id) {
		// TODO Auto-generated method stub
		return recetaRepository.findById(id).orElse(null);
	}

	@Override
	public Receta save(Receta receta) {
		// TODO Auto-generated method stub
		return recetaRepository.save(receta);
	}

	@Override
	public void deleteId(Long id) {
		// TODO Auto-generated method stub
		recetaRepository.deleteById(id);
	}
	

}
