package ed.edu.utpl.pis.sistemamedico.Services;

import ed.edu.utpl.pis.sistemamedico.Interfaces.ICitaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Cita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")

public class CitaServices {
    @Autowired
    private ICitaServices citaService;

    @GetMapping("/cita")
    @ResponseStatus(HttpStatus.OK)
    public List<Cita> getAll(){
        return citaService.findAll();
    }


    @GetMapping("/cita/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cita getById(@PathVariable Long id){
        return citaService.findById(id);
    }

    @PostMapping("/cita")
    @ResponseStatus(HttpStatus.CREATED)
    public Cita save(@RequestBody Cita citas) {
        return citaService.save(citas);
    }

    @PutMapping("/cita/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Cita> updateById(Long id, @Validated @RequestBody Cita citasDetails){
        Cita cita =
                citaService
                        .findById(id);
        cita.setFechaCita(citasDetails.getFechaCita());
        cita.setHoraCita(citasDetails.getHoraCita());
        cita.setDuracionCita(citasDetails.getDuracionCita());
        cita.setDescripcionCita(citasDetails.getDescripcionCita());
        final Cita updatedCita = citaService.save(cita);
        return ResponseEntity.ok(updatedCita);
    }

    @DeleteMapping("/cita/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Boolean> deleteById(Long id){
        citaService.deleteId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
