package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Medico;


public interface MedicoRepository extends CrudRepository<Medico, Long>{
	@Query(value ="SELECT MAX(id) FROM usuario",nativeQuery = true)
    public Long getId();
}
