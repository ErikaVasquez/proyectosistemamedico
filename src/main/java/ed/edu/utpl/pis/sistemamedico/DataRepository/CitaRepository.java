package ed.edu.utpl.pis.sistemamedico.DataRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Cita;
import org.springframework.data.repository.CrudRepository;

public interface CitaRepository extends CrudRepository<Cita, Long> {

}
