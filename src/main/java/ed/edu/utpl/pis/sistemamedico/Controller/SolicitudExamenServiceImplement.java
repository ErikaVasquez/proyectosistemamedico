package ed.edu.utpl.pis.sistemamedico.Controller;
import ed.edu.utpl.pis.sistemamedico.DataRepository.SolicitudExamenRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.ISolicitudExamenServices;
import ed.edu.utpl.pis.sistemamedico.Model.SolicitudExamen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitudExamenServiceImplement implements ISolicitudExamenServices {

    @Autowired
    private SolicitudExamenRepository solicitudExamenRepository;

    public List<SolicitudExamen> findAll() {
        return (List<SolicitudExamen>)solicitudExamenRepository.findAll();
    }

    @Override
    public SolicitudExamen findById(Long id) {
        return solicitudExamenRepository.findById(id).orElse(null);
    }

    @Override
    public SolicitudExamen save(SolicitudExamen solicitudExamen) {
        return solicitudExamenRepository.save(solicitudExamen);
    }

    @Override
    public void deleteId(Long id) {
        solicitudExamenRepository.deleteById(id);
    }

}
