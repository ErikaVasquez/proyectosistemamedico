package ed.edu.utpl.pis.sistemamedico.Interfaces;
import ed.edu.utpl.pis.sistemamedico.Model.SolicitudExamen;

import java.util.List;

public interface ISolicitudExamenServices {
    public List<SolicitudExamen> findAll();
    public SolicitudExamen findById(Long id);
    public SolicitudExamen save(SolicitudExamen solicitudExamen);
    public void deleteId(Long id);

}
