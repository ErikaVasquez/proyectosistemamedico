package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Administrador;

public interface AdministradorRepository extends CrudRepository<Administrador, Long>{
	@Query(value ="SELECT MAX(id) FROM usuario",nativeQuery = true)
    public Long getId();
}