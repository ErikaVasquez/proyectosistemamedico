package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.RolRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IRolServices;
import ed.edu.utpl.pis.sistemamedico.Model.Rol;

@Service
public class RolServiceImplement implements IRolServices{
	
	@Autowired
	private RolRepository rolRepository;

	@Override
	public List<Rol> findAll() {
		return (List<Rol>)rolRepository.findAll();
	}

	@Override
	public Rol findById(Long id) {
		return rolRepository.findById(id).orElse(null);
	}

	@Override
	public Rol save(Rol rol) {
		return rolRepository.save(rol);
	}

	@Override
	public void deleteId(Long id) {
		rolRepository.deleteById(id);
		
	}

}
