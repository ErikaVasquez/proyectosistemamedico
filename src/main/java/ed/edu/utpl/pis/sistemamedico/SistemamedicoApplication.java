package ed.edu.utpl.pis.sistemamedico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemamedicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemamedicoApplication.class, args);
	}

}
