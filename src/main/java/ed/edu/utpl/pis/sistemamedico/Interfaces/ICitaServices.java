package ed.edu.utpl.pis.sistemamedico.Interfaces;

import ed.edu.utpl.pis.sistemamedico.Model.Cita;

import java.util.List;

public interface ICitaServices {

    public List<Cita> findAll();
    public Cita findById(Long id);
    public Cita save(Cita cita);
    public void deleteId(Long id);
}
