package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IAdministradorServices;
import ed.edu.utpl.pis.sistemamedico.Model.Administrador;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class AdministradorServices {
	@Autowired
	private IAdministradorServices administradorService;
	
	@GetMapping("/administrador")
	@ResponseStatus(HttpStatus.OK)
	public List<Administrador> getAll(){
		return administradorService.findAll();
	}
	
	@GetMapping("/administrador/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Administrador getById(@PathVariable(value = "id") Long id){
		return administradorService.findById(id);
	  }
	  
	 @PostMapping("/administrador")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Administrador save(@RequestBody Administrador administrador) {
	    return administradorService.save(administrador);
	 }
	
	 @PutMapping("/administrador/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Administrador> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Administrador administradorDetails){
		Administrador administrador =
				  administradorService
	            .findById(id);
		administrador.setContactoAdmin(administradorDetails.getContactoAdmin());
		administrador.setEmailAdmin(administradorDetails.getEmailAdmin());
	    final Administrador updatedAdmin = administradorService.save(administrador);
	    return ResponseEntity.ok(updatedAdmin);
	  }
	
	  @DeleteMapping("/administrador/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  administradorService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }
	  
	  @GetMapping("/getIdUsuarioAdmin")
		@ResponseStatus(HttpStatus.OK)
		public Long getId(){
			return administradorService.getId();
		}

}
