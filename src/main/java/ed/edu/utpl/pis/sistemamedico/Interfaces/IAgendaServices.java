package ed.edu.utpl.pis.sistemamedico.Interfaces;

import ed.edu.utpl.pis.sistemamedico.Model.Agenda;

import java.util.List;

public interface IAgendaServices {

    public List<Agenda> findAll();
    public Agenda findById(Long id);
    public Agenda save(Agenda agenda);
    public void deleteId(Long id);

}