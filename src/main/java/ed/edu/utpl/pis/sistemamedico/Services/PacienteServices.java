package ed.edu.utpl.pis.sistemamedico.Services;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IPacienteServices;
import ed.edu.utpl.pis.sistemamedico.Model.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")

public class PacienteServices {

    @Autowired
    private IPacienteServices pacienteService;

    @GetMapping("/pacientes")
    @ResponseStatus(HttpStatus.OK)
    public List<Paciente> getAll(){
        return pacienteService.findAll();
    }


    @GetMapping("/pacientes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Paciente getById(@PathVariable(value = "id") Long id){
        return pacienteService.findById(id);
    }

    @PostMapping("/pacientes")
    @ResponseStatus(HttpStatus.CREATED)
    public Paciente save(@RequestBody Paciente pacientes) {
        return pacienteService.save(pacientes);
    }

    @PutMapping("/pacientes/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Paciente> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Paciente pacientessDetails){
        Paciente pacientes =
                pacienteService
                        .findById(id);
        pacientes.setNombrePaciente(pacientessDetails.getNombrePaciente());
        pacientes.setApellidoPaciente(pacientessDetails.getApellidoPaciente());
        pacientes.setCedulaPaciente(pacientessDetails.getCedulaPaciente());
        pacientes.setFechaNacPaciente(pacientessDetails.getFechaNacPaciente());
        pacientes.setContactoPaciente(pacientessDetails.getContactoPaciente());
        pacientes.setGeneroPaciente(pacientessDetails.getGeneroPaciente());
        pacientes.setEmailPaciente(pacientessDetails.getEmailPaciente());
        //pacientes.setReceta(pacientessDetails.getReceta());
        final Paciente updatedPaciente = pacienteService.save(pacientes);
        return ResponseEntity.ok(updatedPaciente);
    }

    @DeleteMapping("/pacientes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
        pacienteService.deleteId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
