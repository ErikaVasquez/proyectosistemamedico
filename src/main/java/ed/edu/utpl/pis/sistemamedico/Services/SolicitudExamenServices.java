package ed.edu.utpl.pis.sistemamedico.Services;

import ed.edu.utpl.pis.sistemamedico.Interfaces.ISolicitudExamenServices;
import ed.edu.utpl.pis.sistemamedico.Model.SolicitudExamen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")

public class SolicitudExamenServices {
    @Autowired
    private ISolicitudExamenServices solicitudExamenService;

    @GetMapping("/solicitudexamen")
    @ResponseStatus(HttpStatus.OK)
    public List<SolicitudExamen> getAll(){
        return solicitudExamenService.findAll();
    }


    @GetMapping("/solicitudexamen/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SolicitudExamen getById(@PathVariable Long id){
        return solicitudExamenService.findById(id);
    }

    @PostMapping("/solicitudexamen")
    @ResponseStatus(HttpStatus.CREATED)
    public SolicitudExamen save(@RequestBody SolicitudExamen solicitudExamen) {
        return solicitudExamenService.save(solicitudExamen);
    }

    @PutMapping("/solicitudexamen/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<SolicitudExamen> updateById(Long id, @Validated @RequestBody SolicitudExamen solicitudExamenDetails){
        SolicitudExamen solicitudExamen =
                solicitudExamenService
                        .findById(id);
        solicitudExamen.setDescripcionSolicitudExamen(solicitudExamenDetails.getDescripcionSolicitudExamen());
        solicitudExamen.setMotivoSolicitudExamen(solicitudExamenDetails.getMotivoSolicitudExamen());
        final SolicitudExamen updatedSolicitudExamen = solicitudExamenService.save(solicitudExamen);
        return ResponseEntity.ok(updatedSolicitudExamen);
    }

    @DeleteMapping("/solicitudexamen/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Boolean> deleteById(Long id){
        solicitudExamenService.deleteId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
