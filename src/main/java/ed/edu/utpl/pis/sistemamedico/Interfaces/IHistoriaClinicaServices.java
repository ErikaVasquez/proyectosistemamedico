package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.HistoriaClinica;

public interface IHistoriaClinicaServices {
	public List<HistoriaClinica> findAll();
	public HistoriaClinica findById(Long id);
	public HistoriaClinica save(HistoriaClinica historiaClinica);
	public void deleteId(Long id);
}
