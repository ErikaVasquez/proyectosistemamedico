package ed.edu.utpl.pis.sistemamedico.Controller;

import ed.edu.utpl.pis.sistemamedico.DataRepository.CitaRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.ICitaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Cita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CitaServiceImplement implements ICitaServices {

    @Autowired
    private CitaRepository citaRepository;

    public List<Cita> findAll() {
        return (List<Cita>)citaRepository.findAll();
    }

    @Override
    public Cita findById(Long id) {
        return citaRepository.findById(id).orElse(null);
    }

    @Override
    public Cita save(Cita cita) {
        return citaRepository.save(cita);
    }

    @Override
    public void deleteId(Long id) {
        citaRepository.deleteById(id);
    }
}
