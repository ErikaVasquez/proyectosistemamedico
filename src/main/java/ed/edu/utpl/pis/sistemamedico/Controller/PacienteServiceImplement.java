package ed.edu.utpl.pis.sistemamedico.Controller;

import ed.edu.utpl.pis.sistemamedico.DataRepository.PacienteRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IPacienteServices;
import ed.edu.utpl.pis.sistemamedico.Model.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacienteServiceImplement implements IPacienteServices {

    @Autowired
    private PacienteRepository pacienteRepository;

    public List<Paciente> findAll() {
        return (List<Paciente>)pacienteRepository.findAll();
    }

    @Override
    public Paciente findById(Long id) {
        return pacienteRepository.findById(id).orElse(null);
    }

    @Override
    public Paciente save(Paciente paciente) {
        return pacienteRepository.save(paciente);
    }

    @Override
    public void deleteId(Long id) {
        pacienteRepository.deleteById(id);
    }


}
