package ed.edu.utpl.pis.sistemamedico.DataRepository;

import ed.edu.utpl.pis.sistemamedico.Model.SolicitudExamen;
import org.springframework.data.repository.CrudRepository;

public interface SolicitudExamenRepository extends CrudRepository<SolicitudExamen, Long> {
}
