package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.UsuarioRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IUsuarioServices;
import ed.edu.utpl.pis.sistemamedico.Model.Usuario;

@Service
public class UsuarioServiceImplement implements IUsuarioServices{

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		return (List<Usuario>)usuarioRepository.findAll();
	}

	@Override
	public Usuario findById(Long id) {
		// TODO Auto-generated method stub
		return usuarioRepository.findById(id).orElse(null);
	}

	@Override
	public Usuario save(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioRepository.save(usuario);
	}

	@Override
	public void deleteId(Long id) {
		usuarioRepository.deleteById(id);
		
	}
	
	@Override
	public Usuario getLogin(String username) {
		Usuario usuario = usuarioRepository.getLogin(username);
		if(usuario==null) {
			throw new UsernameNotFoundException("Could not find user");
		}
		return usuario;
	}

}
