package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Administrador;

public interface IAdministradorServices {
	public List<Administrador> findAll();
	public Administrador findById(Long id);
	public Administrador save(Administrador administrador);
	public void deleteId(Long id);
	public Long getId();
}
