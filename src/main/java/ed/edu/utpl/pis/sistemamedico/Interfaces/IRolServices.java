package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Rol;

public interface IRolServices {
	public List<Rol> findAll();
	public Rol findById(Long id);
	public Rol save(Rol Rol);
	public void deleteId(Long id);
}
