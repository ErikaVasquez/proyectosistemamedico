package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IRecetaServices;
import ed.edu.utpl.pis.sistemamedico.Model.Receta;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class RecetaServices {
	@Autowired
	private IRecetaServices registrarRecetaService;
	
	@GetMapping("/recetas")
    @ResponseStatus(HttpStatus.OK)
    public List<Receta> getAll(){
        return registrarRecetaService.findAll();
    }

    @GetMapping("/recetas/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Receta getById(@PathVariable(value = "id") Long id){
        return registrarRecetaService.findById(id);
    }

    @PostMapping("/recetas")
    @ResponseStatus(HttpStatus.CREATED)
    public Receta save(@RequestBody Receta receta) {
        return registrarRecetaService.save(receta);
    }

    @PutMapping("/recetas/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Receta> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Receta registrarRecetaDetails){
        Receta receta =
        		registrarRecetaService
                        .findById(id);
        receta.setNombreReceta(registrarRecetaDetails.getNombreReceta());
        receta.setDescripcionReceta(registrarRecetaDetails.getDescripcionReceta());
        receta.setFechaReceta(registrarRecetaDetails.getFechaReceta());
        final Receta updatedRegistrarReceta = registrarRecetaService.save(receta);
        return ResponseEntity.ok(updatedRegistrarReceta);
    }

    @DeleteMapping("/recetas/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
    	registrarRecetaService.deleteId(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
