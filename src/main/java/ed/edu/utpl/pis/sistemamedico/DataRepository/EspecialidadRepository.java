package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Especialidad;

public interface EspecialidadRepository extends CrudRepository<Especialidad, Long>{

}
