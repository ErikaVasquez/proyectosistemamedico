package ed.edu.utpl.pis.sistemamedico.DataRepository;

import org.springframework.data.repository.CrudRepository;

import ed.edu.utpl.pis.sistemamedico.Model.HistoriaClinica;

public interface HistoriaClinicaRepository extends CrudRepository<HistoriaClinica, Long>{

}
