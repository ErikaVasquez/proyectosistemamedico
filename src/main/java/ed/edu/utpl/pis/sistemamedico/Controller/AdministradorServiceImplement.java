package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.AdministradorRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IAdministradorServices;
import ed.edu.utpl.pis.sistemamedico.Model.Administrador;

@Service
public class AdministradorServiceImplement implements IAdministradorServices{
	@Autowired
	private AdministradorRepository administradorRepository;

	@Override
	public List<Administrador> findAll() {
		// TODO Auto-generated method stub
		return (List<Administrador>)administradorRepository.findAll();
	}

	@Override
	public Administrador findById(Long id) {
		// TODO Auto-generated method stub
		return administradorRepository.findById(id).orElse(null);
	}

	@Override
	public Administrador save(Administrador medico) {
		// TODO Auto-generated method stub
		return administradorRepository.save(medico);
	}

	@Override
	public void deleteId(Long id) {
		administradorRepository.deleteById(id);
		
	}
	
	public Long getId(){
		Long id = administradorRepository.getId();
		return id;
	}

}
