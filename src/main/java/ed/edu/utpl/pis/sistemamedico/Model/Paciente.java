package ed.edu.utpl.pis.sistemamedico.Model;



import javax.persistence.*;


@Entity
@Table(name = "paciente")
public class Paciente {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "nombre_paciente", nullable = false)
    private String nombrePaciente;

    @Column(name = "apellido_paciente", nullable = false)
    private String apellidoPaciente;

    @Column(name = "cedula_paciente", nullable = false)
    private String cedulaPaciente;

    @Column(name = "fechaNac_paciente", nullable = false)
    private String fechaNacPaciente;

    @Column(name = "contacto_paciente", nullable = true)
    private String contactoPaciente;

    @Column(name = "genero_paciente", nullable = false)
    private String generoPaciente;
    
    @Column(name = "email_paciente", nullable = true)
    private String emailPaciente;
    
    @OneToOne(mappedBy = "paciente")
    private HistoriaClinica historiaClinica;

    public String getEmailPaciente() {
        return emailPaciente;
    }

    public void setEmailPaciente(String emailPaciente) {
        this.emailPaciente = emailPaciente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getApellidoPaciente() {
        return apellidoPaciente;
    }

    public void setApellidoPaciente(String apellidoPaciente) {
        this.apellidoPaciente = apellidoPaciente;
    }

    public String getCedulaPaciente() {
        return cedulaPaciente;
    }

    public void setCedulaPaciente(String cedulaPaciente) {
        this.cedulaPaciente = cedulaPaciente;
    }

    public String getGeneroPaciente() {
        return generoPaciente;
    }

    public void setGeneroPaciente(String generoPaciente) {
        this.generoPaciente = generoPaciente;
    }

    public String getContactoPaciente() {
        return contactoPaciente;
    }

    public void setContactoPaciente(String contactoPaciente) {
        this.contactoPaciente = contactoPaciente;
    }

    public String getFechaNacPaciente() {
        return fechaNacPaciente;
    }

    public void setFechaNacPaciente(String fechaNacPaciente) {
        this.fechaNacPaciente = fechaNacPaciente;
    }
    
}
