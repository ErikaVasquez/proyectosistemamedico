package ed.edu.utpl.pis.sistemamedico.Interfaces;

import ed.edu.utpl.pis.sistemamedico.Model.Paciente;

import java.util.List;

public interface IPacienteServices {

    public List<Paciente> findAll();
    public Paciente findById(Long id);
    public Paciente save(Paciente paciente);
    public void deleteId(Long id);
}
