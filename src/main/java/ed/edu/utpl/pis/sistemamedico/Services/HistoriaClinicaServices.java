package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IHistoriaClinicaServices;
import ed.edu.utpl.pis.sistemamedico.Model.HistoriaClinica;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class HistoriaClinicaServices {
	@Autowired
	private IHistoriaClinicaServices historiaclinicaService;
	
	@GetMapping("/historiaclinica")
	@ResponseStatus(HttpStatus.OK)
	public List<HistoriaClinica> getAll(){
		return historiaclinicaService.findAll();
	}
	
	@GetMapping("/historiaclinica/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public HistoriaClinica getById(@PathVariable(value = "id") Long id){
		return historiaclinicaService.findById(id);
	  }
	  
	 @PostMapping("/historiaclinica")
	 @ResponseStatus(HttpStatus.CREATED)
	 public HistoriaClinica save(@RequestBody HistoriaClinica historiaClinica) {
	    return historiaclinicaService.save(historiaClinica);
	 }
	
	 @PutMapping("/historiaclinica/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<HistoriaClinica> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody HistoriaClinica historiaclinicaDetails){
		HistoriaClinica historiaClinica =
				historiaclinicaService
	            .findById(id);
		historiaClinica.setFechaHistoriaClinica(historiaclinicaDetails.getFechaHistoriaClinica());
		historiaClinica.setMotivoHistoriaClinica(historiaclinicaDetails.getMotivoHistoriaClinica());
		historiaClinica.setTratamientoHistoriaClinica(historiaclinicaDetails.getTratamientoHistoriaClinica());
		historiaClinica.setObservacionHistoriaClinica(historiaclinicaDetails.getObservacionHistoriaClinica());
	    final HistoriaClinica updatedhistoriaClinica = historiaclinicaService.save(historiaClinica);
	    return ResponseEntity.ok(updatedhistoriaClinica);
	  }
	
	  @DeleteMapping("/historiaclinica/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  historiaclinicaService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }

}
