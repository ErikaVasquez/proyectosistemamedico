package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IExamenServices;
import ed.edu.utpl.pis.sistemamedico.Model.Examen;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class ExamenServices {
	
	
	@Autowired
	private IExamenServices examenService;
	
	@GetMapping("/examenes")
	@ResponseStatus(HttpStatus.OK)
	public List<Examen> getAll(){
		return examenService.findAll();
	}
	

	@GetMapping("/examenes/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Examen getById(@PathVariable Long id){
		return examenService.findById(id);
	  }
	  
	 @PostMapping("/examenes")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Examen save(@RequestBody Examen examenes) {
	    return examenService.save(examenes);
	 }
	
	 @PutMapping("/examenes/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Examen> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Examen examenesDetails){
		  Examen examenes =
	        examenService
	            .findById(id);
	    examenes.setNombreExamen(examenesDetails.getNombreExamen());
	    examenes.setCostoExamen(examenesDetails.getCostoExamen());
	    examenes.setDescripcionExamen(examenesDetails.getDescripcionExamen());
	    examenes.setTipoExamen(examenesDetails.getTipoExamen());
	    final Examen updatedExamen = examenService.save(examenes);
	    return ResponseEntity.ok(updatedExamen);
	  }
	
	  @DeleteMapping("/examenes/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  examenService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }
	  
}