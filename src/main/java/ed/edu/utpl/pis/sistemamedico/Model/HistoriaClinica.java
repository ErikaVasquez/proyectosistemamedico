package ed.edu.utpl.pis.sistemamedico.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "historiaclinica")
public class HistoriaClinica {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "fecha_historia_clinica", nullable = false)
    private String fechaHistoriaClinica;
	
	@Column(name = "motivo_historia_clinica", nullable = false)
    private String motivoHistoriaClinica;
	
	@Column(name = "tratamiento_historia_clinica", nullable = false)
    private String tratamientoHistoriaClinica;
	
	@Column(name = "observacion_historia_clinica", nullable = false)
    private String observacionHistoriaClinica;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_paciente", nullable=false)
    private Paciente paciente;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFechaHistoriaClinica() {
		return fechaHistoriaClinica;
	}

	public void setFechaHistoriaClinica(String fechaHistoriaClinica) {
		this.fechaHistoriaClinica = fechaHistoriaClinica;
	}

	public String getMotivoHistoriaClinica() {
		return motivoHistoriaClinica;
	}

	public void setMotivoHistoriaClinica(String motivoHistoriaClinica) {
		this.motivoHistoriaClinica = motivoHistoriaClinica;
	}

	public String getTratamientoHistoriaClinica() {
		return tratamientoHistoriaClinica;
	}

	public void setTratamientoHistoriaClinica(String tratamientoHistoriaClinica) {
		this.tratamientoHistoriaClinica = tratamientoHistoriaClinica;
	}

	public String getObservacionHistoriaClinica() {
		return observacionHistoriaClinica;
	}

	public void setObservacionHistoriaClinica(String observacionHistoriaClinica) {
		this.observacionHistoriaClinica = observacionHistoriaClinica;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
}
