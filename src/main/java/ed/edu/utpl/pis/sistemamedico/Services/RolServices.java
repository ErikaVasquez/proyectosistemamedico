package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IRolServices;
import ed.edu.utpl.pis.sistemamedico.Model.Rol;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class RolServices {
	
	@Autowired
	private IRolServices rolService;
	
	@GetMapping("/roles")
	@ResponseStatus(HttpStatus.OK)
	public List<Rol> getAll(){
		return rolService.findAll();
	}
	
	@GetMapping("/roles/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Rol getById(@PathVariable(value = "id") Long id){
		return rolService.findById(id);
	  }
	  
	 @PostMapping("/roles")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Rol save(@RequestBody Rol rol) {
	    return rolService.save(rol);
	 }
	
	 @PutMapping("/roles/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Rol> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Rol rolDetails){
		  Rol roles =
	        rolService
	            .findById(id);
		roles.setNombreRol(rolDetails.getNombreRol());
		final Rol updatedRoles = rolService.save(roles);
	    return ResponseEntity.ok(updatedRoles);
	  }
	
	  @DeleteMapping("/roles/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  rolService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }

}
