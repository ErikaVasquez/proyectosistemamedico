package ed.edu.utpl.pis.sistemamedico.Model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "solicitudExamen")
public class SolicitudExamen {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "descripcion_solicitudExamen", nullable = false)
    private String descripcionSolicitudExamen;

    @Column(name = "motivo_solicitudExamen", nullable = false)
    private String motivoSolicitudExamen;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_paciente", nullable = false)
    private Paciente paciente;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_medico", nullable = false)
    private Medico medico;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_examen", nullable = false)
    private Examen examen;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescripcionSolicitudExamen() {
        return descripcionSolicitudExamen;
    }

    public void setDescripcionSolicitudExamen(String descripcionSolicitudExamen) {
        this.descripcionSolicitudExamen = descripcionSolicitudExamen;
    }

    public String getMotivoSolicitudExamen() {
        return motivoSolicitudExamen;
    }

    public void setMotivoSolicitudExamen(String motivoSolicitudExamen) {
        this.motivoSolicitudExamen = motivoSolicitudExamen;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Examen getExamen() {
        return examen;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }
}
