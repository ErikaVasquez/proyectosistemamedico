package ed.edu.utpl.pis.sistemamedico.DataRepository;

import ed.edu.utpl.pis.sistemamedico.Model.Paciente;
import org.springframework.data.repository.CrudRepository;

public interface PacienteRepository extends CrudRepository<Paciente, Long> {
}
