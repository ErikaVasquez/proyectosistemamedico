package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IMedicoServices;
import ed.edu.utpl.pis.sistemamedico.Model.Medico;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class MedicoServices {
	@Autowired
	private IMedicoServices medicoService;
	
	@GetMapping("/medicos")
	@ResponseStatus(HttpStatus.OK)
	public List<Medico> getAll(){
		return medicoService.findAll();
	}
	
	@GetMapping("/medicos/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Medico getById(@PathVariable(value = "id") Long id){
		return medicoService.findById(id);
	  }
	  
	 @PostMapping("/medicos")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Medico save(@RequestBody Medico medicos) {
	    return medicoService.save(medicos);
	 }
	
	 @PutMapping("/medicos/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Medico> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Medico medicoDetails){
		  Medico medicos =
	        medicoService
	            .findById(id);
		medicos.setContactoMedico(medicoDetails.getContactoMedico());
		medicos.setEmailMedico(medicoDetails.getEmailMedico());
	    final Medico updatedMedicos = medicoService.save(medicos);
	    return ResponseEntity.ok(updatedMedicos);
	  }
	
	  @DeleteMapping("/medicos/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  medicoService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }
	  
	  @GetMapping("/getIdUsuarioMedico")
		@ResponseStatus(HttpStatus.OK)
		public Long getId(){
			return medicoService.getId();
		}
	  
}
