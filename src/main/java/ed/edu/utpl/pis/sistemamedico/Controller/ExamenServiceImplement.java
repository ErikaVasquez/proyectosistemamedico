package ed.edu.utpl.pis.sistemamedico.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ed.edu.utpl.pis.sistemamedico.DataRepository.ExamenRepository;
import ed.edu.utpl.pis.sistemamedico.Interfaces.IExamenServices;
import ed.edu.utpl.pis.sistemamedico.Model.Examen;

@Service
public class ExamenServiceImplement implements IExamenServices{
	
	@Autowired
	private ExamenRepository examenRepository;
	
	@Override
	public List<Examen> findAll() {
		return (List<Examen>)examenRepository.findAll();
	}

	@Override
	public Examen findById(Long id) {
		return examenRepository.findById(id).orElse(null);
	}

	@Override
	public Examen save(Examen examen) {
		return examenRepository.save(examen);
	}

	@Override
	public void deleteId(Long id) {
		examenRepository.deleteById(id);
	}
	
	

}
