package ed.edu.utpl.pis.sistemamedico.Services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ed.edu.utpl.pis.sistemamedico.Interfaces.IEspecialidadServices;
import ed.edu.utpl.pis.sistemamedico.Model.Especialidad;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
@RequestMapping("/api/v1")
public class EspecialidadServices {
	
	@Autowired
	private IEspecialidadServices especialidadService;
	
	@GetMapping("/especialidades")
	@ResponseStatus(HttpStatus.OK)
	public List<Especialidad> getAll(){
		return especialidadService.findAll();
	}
	

	@GetMapping("/especialidades/{id}")
	@ResponseStatus(HttpStatus.OK)
	  public Especialidad getById(@PathVariable Long id){
		return especialidadService.findById(id);
	  }
	  
	 @PostMapping("/especialidades")
	 @ResponseStatus(HttpStatus.CREATED)
	 public Especialidad save(@RequestBody Especialidad especialidad) {
	    return especialidadService.save(especialidad);
	 }
	
	 @PutMapping("/especialidades/{id}")
	 @ResponseStatus(HttpStatus.CREATED)
	 public ResponseEntity<Especialidad> updateById(@PathVariable(value = "id") Long id, @Validated @RequestBody Especialidad examenesDetails){
		 Especialidad especialidad =
				 especialidadService
	            .findById(id);
		especialidad.setNombreEspecialidad(examenesDetails.getNombreEspecialidad());
	    final Especialidad updatedEspecialidad = especialidadService.save(especialidad);
	    return ResponseEntity.ok(updatedEspecialidad);
	  }
	
	  @DeleteMapping("/especialidades/{id}")
	  @ResponseStatus(HttpStatus.OK)
	  public Map<String, Boolean> deleteById(@PathVariable(value = "id") Long id){
		  especialidadService.deleteId(id);
	    Map<String, Boolean> response = new HashMap<>();
	    response.put("deleted", Boolean.TRUE);
	    return response;
	  }

}
