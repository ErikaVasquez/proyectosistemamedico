package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Examen;

public interface IExamenServices {
	public List<Examen> findAll();
	public Examen findById(Long id);
	public Examen save(Examen examen);
	public void deleteId(Long id);
}