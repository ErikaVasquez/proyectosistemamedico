package ed.edu.utpl.pis.sistemamedico.Interfaces;

import java.util.List;

import ed.edu.utpl.pis.sistemamedico.Model.Usuario;

public interface IUsuarioServices {
	public List<Usuario> findAll();
	public Usuario findById(Long id);
	public Usuario save(Usuario usuario);
	public void deleteId(Long id);
	public Usuario getLogin(String username);
}
