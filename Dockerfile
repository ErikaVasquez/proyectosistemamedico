FROM openjdk:8
VOLUME /tmp
EXPOSE 9000
ADD target/*.jar servicio-examen.jar
ENV JAVA_OPTS =""
ENTRYPOINT ["java","-jar","/servicio-examen.jar" ]